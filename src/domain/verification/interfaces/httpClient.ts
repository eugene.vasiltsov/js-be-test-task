import { RequestOption } from "./requestOption";
import { ResponseResult } from "./responseResult";

export interface HttpClient {
  get(url: string, options?: RequestOption): Promise<ResponseResult>;
}
