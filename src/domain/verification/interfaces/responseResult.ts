export interface ResponseResult {
  isSuccess: boolean;
  statusCode: number;
  data: any;
  url: string;
}
