import { HttpClientService } from "../http/httpClientService";
import { HttpClient } from "./interfaces/httpClient";
import { retryPromise } from "../../helpers/retry";
import { NotFound } from "../../helpers/errors";

enum DocumentContext {
  FRONT = "document-front",
  BACK = "document-back",
}
enum ContextTypes {
  FRONT = "front",
  BACK = "back",
}
const relevantContextTypes = [ContextTypes.FRONT, ContextTypes.BACK];

export default class VerificationService {
  static url = "https://api.veriff.internal/api";

  private httpClient: HttpClient;

  private sessionId: string;

  private relevantMedia: [];

  constructor(sessionId: string) {
    this.httpClient = HttpClientService;
    this.sessionId = sessionId;
    this.relevantMedia = [];
  }

  async getSession() {
    const result = await retryPromise(
      this.httpClient.get.bind(
        null,
        `${VerificationService.url}/sessions/${this.sessionId}`
      )
    );
    if (!result.isSuccess && result.statusCode === 404) {
      throw new NotFound("session not found");
    }
    return result;
  }

  async getMedia() {
    const result = await retryPromise(
      this.httpClient.get.bind(
        null,
        `${VerificationService.url}/sessions/${this.sessionId}/media`
      )
    );
    if (!result.isSuccess) {
      result.data = [];
    }
    return result;
  }

  async getMediaContext() {
    const result = await retryPromise(
      this.httpClient.get.bind(
        null,
        `${VerificationService.url}/media-context/${this.sessionId}`
      )
    );
    if (!result.isSuccess) {
      result.data = [];
    }
    return result;
  }

  async getRelevant() {
    await this.getSession();
    const [allMedia, allContext] = await Promise.all([
      this.getMedia(),
      this.getMediaContext(),
    ]);

    this.relevantMedia = allMedia.data.reduce(
      (acc, cur) => {
        const context = allContext.data.find(
          (context) => cur.id === context.mediaId
        );

        if (context && relevantContextTypes.includes(context.context)) {
          if (context.context === ContextTypes.FRONT) {
            acc[ContextTypes.FRONT].push({
              ...cur,
              ...{ probability: context.probability },
            });
          } else if (context.context === ContextTypes.BACK) {
            acc[ContextTypes.BACK].push({
              ...cur,
              ...{ probability: context.probability },
            });
          }
        }
        return acc;
      },
      {
        [ContextTypes.FRONT]: [],
        [ContextTypes.BACK]: [],
      }
    );

    // sort
    this.sortByProbability();

    return this.relevantMedia;
  }

  sortByProbability() {
    if (this.relevantMedia[ContextTypes.BACK]) {
      this.relevantMedia[ContextTypes.BACK] = this.relevantMedia[
        ContextTypes.BACK
      ].sort((a, b) => {
        if (a.probability > b.probability) {
          return 1;
        }
        if (a.probability < b.probability) {
          return -1;
        }
        return 0;
      });
    }
    if (this.relevantMedia[ContextTypes.FRONT]) {
      this.relevantMedia[ContextTypes.FRONT] = this.relevantMedia[
        ContextTypes.FRONT
      ].sort((a, b) => {
        if (a.probability < b.probability) {
          return 1;
        }
        if (a.probability > b.probability) {
          return -1;
        }
        return 0;
      });
    }
  }
}
