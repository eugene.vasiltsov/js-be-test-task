import { isUUID } from "../../validators/isUUID";
import VerificationService from "./verificationService";
import { BadRequest } from "../../helpers/errors";

const getRelevantMedia = async (req, res) => {
  const sessionId = req.params.sessionId;
  if (!isUUID(sessionId)) {
    throw new BadRequest("sessionId not valid");
  }

  const verificationService = new VerificationService(sessionId);
  const relevantMedia = await verificationService.getRelevant();

  return res.status(200).json({ data: relevantMedia });
};

export default getRelevantMedia;
