import axios from "axios";

export class HttpClientService {
  public static get(
    url: string,
    options: {
      headers?: any;
      body?: any;
    } = {}
  ): Promise<{
    isSuccess: boolean;
    statusCode: number;
    data: any;
    url: string;
  }> {
    const headersDefault = {};
    const headers = { ...options.headers, ...headersDefault };

    return axios({
      method: "get",
      url,
      headers,
    })
      .then(function (response) {
        return {
          isSuccess: true,
          data: response.data,
          statusCode: response.status,
          url,
        };
      })
      .catch((err) => {
        if (err.response === undefined) {
          return {
            isSuccess: false,
            data: null,
            statusCode: 500,
            url,
          };
        }

        return {
          isSuccess: false,
          data: err.response.data,
          statusCode: err.response.status,
          url,
        };
      });
  }
}
