import { NextFunction, Request, Response } from "express";
import { CustomError } from "./errors";

const handleError = function handleError(
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
) {
  let customError = err;

  if (!err?.status) {
    customError = new CustomError("Oops something went wrong", 500);
  }
  console.error("error", err);
  res.status((customError as CustomError).status).send(customError);
};

export default handleError;
