const wait = (interval) => new Promise((resolve) => setTimeout(resolve));

export async function retryPromise(fn, retriesLeft = 5, interval = 200) {
  const result = await fn();

  if (result.isSuccess || result.statusCode < 500) {
    return result;
  }

  if (retriesLeft < 1) {
    console.debug(`Maximum retries exceeded!: ${result.url}`, retriesLeft);
    return result;
  }

  console.debug(`retriesLeft: ${result.url}`, retriesLeft);

  await wait(interval);

  return retryPromise(fn, --retriesLeft);
}
