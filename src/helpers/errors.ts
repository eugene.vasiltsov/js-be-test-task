export class CustomError extends Error {
  name: string;
  message: string;
  status: number;
  constructor(message, status) {
    super();

    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;

    this.message = message || "Something went wrong. Please try again.";

    this.status = status || 500;
  }
}

export class NotFound extends CustomError {
  constructor(message) {
    super(message, 404);
    this.name = "NotFound";
    this.message = message;
  }
}

export class BadRequest extends CustomError {
  constructor(message) {
    super(message, 400);
    this.name = "BadRequest";
    this.message = message;
  }
}
