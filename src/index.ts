import express from "express";
import "./externalService";
import getRelevantMedia from "./domain/verification/verificationController";
import asyncHandler from "./helpers/asyncHandler";
import handleError from "./helpers/handleError";

const app = express();
const port = 3000;

app.get("/api/sessions/:sessionId", asyncHandler(getRelevantMedia));

app.use(handleError);
app.listen(port, () => {
  console.info(`Service is listening at http://localhost:${port}`);
});
