import { uuidRegexp } from "../helpers/regexps";

export const isUUID = (value: string): boolean => {
  return uuidRegexp.test(value);
};
