import { HttpClient } from "../src/domain/verification/interfaces/httpClient";
import { HttpClientService } from "../src/domain/http/httpClientService";

describe("GET http://localhost:3000/api/sessions/:sessionId", () => {
  let httpClient: HttpClient;
  let apiUrl: string;

  beforeAll(() => {
    httpClient = HttpClientService;
    apiUrl = "http://localhost:3000/api/sessions";
  });

  it("should return 400 bad request", async () => {
    const response = await httpClient.get(`${apiUrl}/wrong-session-string`);
    expect(response.statusCode).toEqual(400);
  });

  it("should return 404 not found", async () => {
    const response = await httpClient.get(
      `${apiUrl}/90d61876-b99a-443e-994c-ba882c8558b1`
    );
    expect(response.statusCode).toEqual(404);
  });

  it("should return session with media", async () => {
    const response = await httpClient.get(
      `${apiUrl}/90d61876-b99a-443e-994c-ba882c8558b6`
    );
    expect(response.statusCode).toEqual(200);
    expect(response.data).toMatchObject({
      data: {
        front: [
          {
            id: "40851916-3e86-45cd-b8ce-0e948a8a7751",
            mimeType: "image/png",
            context: "document-front",
            probability: 0.9264236,
          },
          {
            id: "7f2dcbd8-5b5f-4f1a-bfa4-016ddf4dd662",
            mimeType: "image/png",
            context: "document-front",
            probability: 0.8734357,
          },
          {
            id: "40f1e462-6db8-4313-ace3-83e4f5619c56",
            mimeType: "image/png",
            context: "document-back",
            probability: 0.2931033,
          },
        ],
        back: [
          {
            id: "a6c90b4f-ddfc-49eb-89ad-05b7f1274f96",
            mimeType: "image/png",
            context: "document-front",
            probability: 0.9739324,
          },
        ],
      },
    });
  });
});
